# GoogleBuillingUtil
基于com.android.billingclient:billing:1.2，对整个支付流程进行封装。

**推荐一款我写的全平台广告快速集成框架【ADEasy】:[https://blog.csdn.net/u013640004/article/details/105416193](https://blog.csdn.net/u013640004/article/details/105416193)**

该版本只适合在单activity使用，否则需要处理监听器清除的问题，建议尝试使用最新版：[GoogleBilling1.2.2.13 ](https://github.com/TJHello/GoogleBilling)
- 1、将所有接口统一到OnGoogleBillingListener，从此只需要设置一个监听器就能选择性监听所有事件了。
- 2、优化逻辑，条理更清晰，解决了旧版本带来的监听器设置与清除逻辑混乱的问题。
- 3、添加在线订单查询方法，能够获取所有历史订单(但需要注意，获取的订单暂时无法判断其有效性)

QQ群交流：425219113(计算机语言交流)

=============使用方法==================

- 下载GoogleBuillingUtil

- API接入

1. AndroidStudio
```groovy

==========build.gradle(Module)==========

android {
    dependencies {
        implementation 'com.android.billingclient:billing:1.2.2'
        //如果用到isGooglePlayServicesAvailable方法需要导入这个包，这个方法也可以去掉。
        implementation "com.google.android.gms:play-services-location:16.0.0"
    }
}

```
2. Eclipse:
我的博客里面有提取的billing:1.0.jar可以下载使用，记得添加activity说明，博客地址见最下面的版本描述。

- 设置支付权限


```
//aar里面自带权限了，app可以不理会，但其他集成方式需要确保有这个权限。
<uses-permission
        android:name="com.android.vending.BILLING"
        android:required="false"/>
```


- 商品Id配置(必须)

```java
//在GoogleBillingUtil.build之前调用。只需要设置一次。
GoogleBillingUtil.setSkus(new String[]{"内购商品id"},new String[]{"订阅商品id"})//没有的时候传null或者new String[]{},而不是new String[]{""}

```


- 代码示例

```java
private GoogleBillingUtil googleBillingUtil = null;
private MyOnPurchaseFinishedListener mOnPurchaseFinishedListener = new MyOnPurchaseFinishedListener();//购买回调接口
private MyOnQueryFinishedListener mOnQueryFinishedListener = new MyOnQueryFinishedListener();//查询回调接口
private MyOnStartSetupFinishedListener mOnStartSetupFinishedListener = new MyOnStartSetupFinishedListener ();//启动结果回调接口
private MyOnConsumeResponseListener mOnConsumeResponseListener = new MyOnConsumeResponseListener();//消耗结果回调接口

```
1. 创建示例,需要在build之前设置回调接口,接口可以选择性设置

```java
@Override
protected void onCreate(...) {
    super.onCreate(...);
        GoogleBillingUtil.setIsAutoConsumeAsync(false);//建议有服务端验证的情况下不要打开自动消耗，而是应该自己实现消耗的相关逻辑。
	googleBillingUtil = GoogleBillingUtil.getInstance()
                .setOnPurchaseFinishedListener(mOnPurchaseFinishedListener)//注意，监听器设置是可选的，视个人需求设置。
                .setOnQueryFinishedListener(mOnQueryFinishedListener)
                .setOnStartSetupFinishedListener(mOnStartSetupFinishedListener)
                .setOnConsumeResponseListener(mOnConsumeResponseListener )
                .build(context);
}

```

2. 发起内购或者订阅


```java
public void queryInventoryInApp() 查询内购商品信息列表
public void queryInventorySubs() 查询订阅商品信息列表
public void purchaseInApp(Activity activity,String skuId) 发起内购
public void purchaseSubs(Activity activity,String skuId) 发起订阅
```

3. 清除监听器。


```
//如果下个页面或者上个页面没有使用到googleBuillingUtil.getInstance()，那么就需要在finish或者startActivity之前调用cleanListener()方法，来清除接口。
//可以尝试这样
@Override
    public void onBackPressed() {
        GoogleBillingUtil.cleanListener();
        super.onBackPressed();
    }
//返回键点击
public void onBackClick()
    {
        GoogleBillingUtil.cleanListener();
        this.finish();
    }


//如果只使用一次googleBuillingUtil，可以选择使用endConnection()方法断开google服务的连接，下次使用重新连接。


```


4. 接口说明

```
//查询商品信息回调接口
private class MyOnQueryFinishedListener implements GoogleBillingUtil.OnQueryFinishedListener
{
  @Override
  public void onQuerySuccess(String skuType,List<SkuDetails> list) {
      //查询成功，返回商品列表，
      //skuDetails.getPrice()获得价格(文本)
      //skuDetails.getType()获得类型 sub或者inapp,因为sub和inapp的查询结果都走这里，所以需要判断。
      //googleBillingUtil.getSubsPositionBySku(skuDetails.getSku())获得当前subs sku的序号
      //googleBillingUtil.getInAppPositionBySku(skuDetails.getSku())获得当前inapp suk的序号
  }
  @Override
  public void onQueryFail(int responseCode) {
      //查询失败
  }

  @Override
  public void onQueryError() {
      //查询错误
  }
}
//服务初始化结果回调接口
private class MyOnStartSetupFinishedListener implements GoogleBillingUtil.OnStartSetupFinishedListener
{
//...;
}
//购买商品回调接口
private class MyOnPurchaseFinishedListener implements GoogleBillingUtil.OnPurchaseFinishedListener
{
     @Override
        public void onPurchaseSuccess(List<Purchase> list) {
            //内购或者订阅成功,可以通过purchase.getSku()获取suk进而来判断是哪个商品
        }

        @Override
        public void onPurchaseFail(int responseCode) {
            
        }


        @Override
        public void onPurchError() {
            
        }
}

```

==================响应代码汇总([官方地址](https://developer.android.com/google/play/billing/billing_reference))=============


| 响应代码                                    | 值 | 说明                                                                                                                                 |
| ------------------------------------------- | -- | ------------------------------------------------------------------------------------------------------------------------------------ |
| BILLING_RESPONSE_RESULT_OK                  | 0  | 成功                                                                                                                                  |
| BILLING_RESPONSE_RESULT_USER_CANCELED       | 1  | 用户按上一步或取消对话框                                                                                                             |
| BILLING_RESPONSE_RESULT_SERVICE_UNAVAILABLE | 2  | 网络连接断开                                                                                                                         |
| BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE | 3  | 所请求的类型不支持 Billing API 版本(支付环境问题)                                                                                         |
| BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE    | 4  | 请求的商品已不再出售。                                                                                                               |
| BILLING_RESPONSE_RESULT_DEVELOPER_ERROR     | 5  | 提供给 API 的参数无效。此错误也可能说明未在 Google Play 中针对应用内购买结算正确签署或设置应用，或者应用在其清单中不具备所需的权限。 |
| BILLING_RESPONSE_RESULT_ERROR               | 6  | API 操作期间出现严重错误                                                                                                             |
| BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED  | 7  | 未能购买，因为已经拥有此商品                                                                                                         |
| BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED      | 8  | 未能消费，因为尚未拥有此商品                                                                                                         |


==================常见问题=============

 **1. 初始化失败，错误码:3，这是支付环境问题。** 

有以下可能：用的是模拟器，三件套版本太旧，应用的支付环境没配置(接入谷歌服务，支付权限)，vpn地域不支持。

解决方法：a.先验证环境。在商店下载一个有内购的应用，看能否进行内购。b.如果别人的能进行内购之后，再次测试你的应用，看是否正常，来确认应用的支付环境是否正常。

 **2. 能够查询价格，但无法购买，提示"商品无法购买"之类。** 

这是基础配置问题，有以下可能：版本号与线上版本不对应，测试版本却不是测试账号(大概率)，签名不对应。

 **3. 能够查询价格，但无法调起支付都没有弹窗，错误码:3，报错：Error:In-app billing error: Null data in IAB activity resul。** 

原因是没有给Google play商店弹窗权限，国内很多手机都有弹窗权限管理，特别是小米，如果没允许，是不会有任何提示，并且拦截了的。(这个问题在新版的gp商店已经不存在）

 **4. 支付提示成功，但却走onQueryFail回调，并且返回的商品列表为null。** 

这是因为你调错了方法，记得purchaseInApp是内购的，purchaseSubs是订阅的。查询的时候同理。另外查询的时候报错，很有可能是你setSKUS的时候传了一个空字符串，而不是空数组。

 **5. 查询的时候返回的商品列表长度为0。** 

setSkus的时候将内购sku和订阅sku的参数顺序弄错了，应该是第一个是内购的，第二个参数是订阅的。

或者是商品还没有发布成功，需要等待一段时间(很有可能，新发布的商品是无论怎么查询还是购买，谷歌那边都是没有响应的)


 **6. 我们检测到您的应用使用的是旧版 Google Play Developer API。自 2019 年 12 月 1 日起，
 我们将不再支持此 API 的版本 1 和版本 2。请在该日期之前将您使用的 API 更新到版本 3。请注意，此变动与弃用 AIDL/结算库无关。**

 升级到com.android.billingclient:billing库，弃用AIDL相关代码。

 后台也要弃用v3的校验接口。具体见谷歌官方文档。


==================API说明=============

```
//初始化
1、public static GoogleBillingUtil getInstance() 获取单实例
2、public GoogleBillingUtil build() 
	创建内购实例，连接谷歌支付服务(如果未创建、未连接)，并查询商品信息列表。
	如果默认自动消耗已内购但未被消耗的商品，可以通过设置isAutoConsumeAsync改变。
3、public void startConnection() 
	连接谷歌支付服务(一般情况下不需要手动调用，build的时候已经调用了)
	

//查询、购买与消耗
4、public void queryInventoryInApp() 查询内购商品信息列表
5、public void queryInventorySubs() 查询订阅商品信息列表
6、public void purchaseInApp(Activity activity,String skuId) 发起内购
7、public void purchaseSubs(Activity activity,String skuId) 发起订阅
8、public void consumeAsync(String purchaseToken) 消耗商品(一般情况下不需要手动调用，内购的时候自动调用了)

//购买历史、有效订阅数
9、public List<Purchase> queryPurchasesInApp() 获取已经内购的商品列表
10、public List<Purchase> queryPurchasesSubs() 获取已经订阅的商品列表
11、public int getPurchasesSizeSubs() 获取有效订阅的数量(-1查询失败，0没有有效订阅，>0具有有效的订阅)

//便捷工具
12、public int getSubsPositionBySku(String sku) 通过sku获取订阅商品序号
13、public int getInAppPositionBySku(String sku) 通过sku获取内购商品序号
14、public String getSubsSkuByPosition(int position) 通过序号获取订阅sku
15、public String getInAppSkuByPosition(int position) 通过序号获取内购sku
16、private String getSkuType(String sku) 通过sku获取商品类型(订阅获取内购)inapp内购，subs订阅

//接口设置
17、public GoogleBillingUtil setOnPurchaseFinishedListener(OnPurchaseFinishedListener onPurchaseFinishedListener) 购买回调接口
18、public GoogleBillingUtil setOnQueryFinishedListener(OnQueryFinishedListener onQueryFinishedListener) 商品信息查询接口
19、public GoogleBillingUtil setOnStartSetupFinishedListener(OnStartSetupFinishedListener onStartSetupFinishedListener) 服务初始化结果回调接口 

//其他、内存
20、public void setIsAutoConsumeAsync(boolean isAutoConsumeAsync) 设置是否自动消耗商品，慎用
21、public static void cleanListener() 清除所有监听器，避免内存泄漏，回调错乱等问题。
22、public static void endConnection() 断开连接google服务（一般情况不建议调用该方法，让google保留连接是最好的选择。）

```


================版本说明===============
/**
 * Created by TJbaobao on 2017/11/2.
 * CSDN:http://blog.csdn.net/u013640004/article/details/78257536
 *
 * 当前版本:V1.1.7
 * 更新日志:
 *
 * v1.1.7 2019/01/05
 * 开放消耗结果监听器设置-{@link #setOnConsumeResponseListener}
 *
 * v1.1.6 2018/09/05
 * 去掉我项目了的BaseApplication.getContext()的方法，初始现在需要传入一个Context，可以使用Application的Context
 * 对isGooglePlayServicesAvailable方法进行了说明，因为这个方法是要导入一个包才能使用的。
 * --> api "com.google.android.gms:play-services-location:11.8.0"
 *
 * v1.1.5 2018/07/13
 * 优化-尽可能处理了一些可能造成的内存泄漏的问题。
 * 修改-查询成功接口增加一个String skuType，参数，各位在查询的时候需要判断skuType
 * 增加-增加两处接口为Null的Log提示，tag为GoogleBillingUtil。
 *
 * V1.1.4 2018/01/03
 * 修改-实现单例模式，避免多实例导致的谷歌接口回调错乱问题。
 *
 * V1.1.3 2017/12/19
 * 修复-服务启动失败时导致的空指针错误。
 *
 * V1.1.2    2017/12/18
 * 修复-修复内购未被消耗的BUG。
 * 增加-每次启动都获取一次历史内购订单，并且全部消耗。
 * 增加-可以通过设置isAutoConsumeAsync来确定内购是否每次自动消耗。
 * 增加-将consumeAsync改为public，你可以手动调用消耗。
 *
 * V1.1.1  2017/11/2
 * 升级-内购API版本为google最新版本。compile 'com.android.billingclient:billing:1.0'
 * 特性-不需要key了，不需要IInAppBillingService.aidl了，不需要那一大堆Utils了，创建新实例的时候必须要传入购买回调接口。
 *
 * V1.0.3 2017/10/27
 * 增加-支持内购
 *
 * V1.0.2  2017/09/11
 * 修复-修复BUG
 *
 * v1.0.1 2017/07/29
 * 初始版本
 */